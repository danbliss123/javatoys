package danbliss.circlepuzzle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Circle {

	public static enum RecordType {
		INTERSECTION, MIDDLE, EDGE;
	}
	
	private final List<CirclePoint> points;
	private final int radiusSquared;
	private final int count;
	private final RecordType type;
	public Circle(List<CirclePoint> points, int radius, int count,
			RecordType type) {
		super();
		this.points = Collections.unmodifiableList(new ArrayList<CirclePoint>(points));
		this.radiusSquared = radius;
		this.count = count;
		this.type = type;
	}
	public List<CirclePoint> getPoints() {
		return points;
	}
	public int getRadiusSquared() {
		return radiusSquared;
	}
	public int getCount() {
		return count;
	}
	public RecordType getType() {
		return type;
	}
	public double getRadius() {
		if(type==RecordType.INTERSECTION) {
			return Math.sqrt(radiusSquared);
		} else {
			return Math.sqrt(radiusSquared)/2;
		}
	}
	
	
	
	
}
