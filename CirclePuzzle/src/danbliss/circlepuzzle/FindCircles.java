package danbliss.circlepuzzle;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Vector;


public class FindCircles {
	
	@SuppressWarnings("unchecked")
	public static List<Circle>[] listCircles(int maxRadiusSquared) {
		// Record keeping variables
		int pointCountOnIntersectionRecord = 0;
		List<Circle> pointCountOnIntersectionRecordHolders = new Vector<>();
		int pointCountInMiddleRecord = 0;
		List<Circle> pointCountInMiddleRecordHolders = new Vector<>();
		int pointCountOnEdgeRecord = 0;
		List<Circle> pointCountOnEdgeRecordHolders = new Vector<>();

		// This queue is the primary data structure
		// It always contains a list of CirclePoints with r^2 greater
		// than or equal to the current r^2
		Queue<CirclePoint> pointQueue = new PriorityQueue<>();
		
		// It starts with just the the point 1,0
		pointQueue.add(new CirclePoint(1));
		
		// The current r^2
		int circleRadiusSquared = 0;
		
		// Points in the current circle
		List<CirclePoint> circlePoints = new ArrayList<>(3);

		// While the current r^2 is still small enough
		while(circleRadiusSquared<maxRadiusSquared) {
			
			// Remove the point with the smallest r^2
			// aka the closest point
			CirclePoint closestPoint = pointQueue.remove();
			
			// if that point was the first one with that large coordinate,
			// it is time to add the next large coordinate
			if(closestPoint.getSmallCoordinate()==0) {
				pointQueue.add(new CirclePoint(closestPoint.getLargeCoordinate()+1));
			}
			// if that point was the last entry in its row (largec==smallc), then
			// do not add anything.  Otherwise add the next point with the same
			// large coordinate
			if(closestPoint.getLargeCoordinate() > closestPoint.getSmallCoordinate()) {
				pointQueue.add(closestPoint.incrementSmallCoordinate());
			}
			
			// if this point belongs on the current circle, simply add it.
			if(closestPoint.getRadiusSquared() == circleRadiusSquared) {
				circlePoints.add(closestPoint);
			} else {
				// if the point is further away than the current circle, it is time
				// to analyze the previous circle and prepare for the next circle.
				// Record the final information about the current circle.
				List<CirclePoint> fullCirclePoints = circlePoints;
				int fullCircleRadiusSquared = circleRadiusSquared;
				
				// Clear the current information to make room for the next circle
				circlePoints = new ArrayList<>(3);
				circlePoints.add(closestPoint);
				circleRadiusSquared = closestPoint.getRadiusSquared();
				
				// Count how many points the current circle would have with the various placements.
				int pointCountOnIntersection = 0;
				int pointCountInMiddle = 0;
				int pointCountOnEdge = 0;
				
				if(fullCirclePoints.size()*4 >= pointCountOnEdgeRecord) {
					for(CirclePoint fullCirclePoint : fullCirclePoints) {
						if(fullCirclePoint.getSmallCoordinate() == 0) {
							pointCountOnIntersection += 4;
						} else if(fullCirclePoint.getSmallCoordinate() == fullCirclePoint.getLargeCoordinate()) {
							pointCountOnIntersection += 4;
						} else {
							pointCountOnIntersection += 8;
						}
						if(fullCirclePoint.getSmallCoordinate()%2==1 && fullCirclePoint.getLargeCoordinate()%2==1 ) {
							if(fullCirclePoint.getSmallCoordinate() == fullCirclePoint.getLargeCoordinate()) {
								pointCountInMiddle += 4;
							} else {
								pointCountInMiddle += 8;
							}
						}
						if((fullCirclePoint.getSmallCoordinate() + fullCirclePoint.getLargeCoordinate())%2 == 1 ) {
							if(fullCirclePoint.getSmallCoordinate()==0 || fullCirclePoint.getLargeCoordinate()==0) {
								pointCountOnEdge += 2;
							} else {
								pointCountOnEdge += 4;
							}
						}
					}
					
					// If the circle beats any records, record it
					if(pointCountOnIntersection > pointCountOnIntersectionRecord) {
						pointCountOnIntersectionRecord = pointCountOnIntersection;
						pointCountOnIntersectionRecordHolders.add(new Circle(
								fullCirclePoints,
								fullCircleRadiusSquared,
								pointCountOnIntersection,
								Circle.RecordType.INTERSECTION
								));
					}
					if(pointCountInMiddle > pointCountInMiddleRecord) {
						pointCountInMiddleRecord = pointCountInMiddle;
						pointCountInMiddleRecordHolders.add(new Circle(
								fullCirclePoints,
								fullCircleRadiusSquared,
								pointCountInMiddle,
								Circle.RecordType.MIDDLE
								));
					}
					if(pointCountOnEdge > pointCountOnEdgeRecord) {
						pointCountOnEdgeRecord = pointCountOnEdge;
						pointCountOnEdgeRecordHolders.add(new Circle(
								fullCirclePoints,
								fullCircleRadiusSquared,
								pointCountOnEdge,
								Circle.RecordType.EDGE
								));
					}

				}
				
			}
			
		}
		
		return new List[]{
			pointCountOnIntersectionRecordHolders, 
			pointCountInMiddleRecordHolders, 
			pointCountOnEdgeRecordHolders};

	}

	
	public static void main(String[] args) {
		
		long timeStart = System.currentTimeMillis();
		
		List<Circle>[] results = listCircles(100000000);
		
		long timeEnd = System.currentTimeMillis();
		
		System.out.println( (timeEnd-timeStart)/1000f );
		
		System.out.println("Circles centered on grid intersections:");
		for(Circle record : results[0]) {
			System.out.format("%5d: %.5f\n", record.getCount(), record.getRadius());
			for(CirclePoint point : record.getPoints()) {
				System.out.format("        %d, %d\n", point.getLargeCoordinate(), point.getSmallCoordinate());
			}
		}
		System.out.println("\nCircles centered on grid squares:");
		for(Circle record : results[1]) {
			System.out.format("%5d: %.5f\n", record.getCount(), record.getRadius());
			for(CirclePoint point : record.getPoints()) {
				System.out.format("        %d, %d\n", point.getLargeCoordinate(), point.getSmallCoordinate());
			}
		}
		System.out.println("\nCircles centered on grid edges:");
		for(Circle record : results[2]) {
			System.out.format("%5d: %.5f\n", record.getCount(), record.getRadius());
			for(CirclePoint point : record.getPoints()) {
				System.out.format("        %d, %d\n", point.getLargeCoordinate(), point.getSmallCoordinate());
			}
		}

		
	}
	
	
	
	
}
