package danbliss.circlepuzzle;

public class CirclePoint implements Comparable<CirclePoint> {

	private final int largeCoordinate;
	private final int smallCoordinate;
	private final int radiusSquared;
	
	public CirclePoint(int largeCoordinate) {
		super();
		this.largeCoordinate = largeCoordinate;
		smallCoordinate = 0;
		radiusSquared = largeCoordinate*largeCoordinate;
	}
	
	private CirclePoint(int largeCoordinate, int smallCoordinate,
			int radiusSquared) {
		super();
		this.largeCoordinate = largeCoordinate;
		this.smallCoordinate = smallCoordinate;
		this.radiusSquared = radiusSquared;
	}

	public int getLargeCoordinate() {
		return largeCoordinate;
	}
	public int getSmallCoordinate() {
		return smallCoordinate;
	}
	public int getRadiusSquared() {
		return radiusSquared;
	}
	public CirclePoint incrementSmallCoordinate() {
		return new CirclePoint(largeCoordinate, smallCoordinate+1, radiusSquared + smallCoordinate + smallCoordinate + 1);
	}
	@Override
	public int compareTo(CirclePoint arg0) {
		int returnvalue = Integer.compare(radiusSquared, arg0.radiusSquared);
		if(returnvalue == 0) {
			returnvalue = Integer.compare(largeCoordinate, arg0.largeCoordinate);
		}
		return returnvalue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + largeCoordinate;
		result = prime * result + smallCoordinate;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CirclePoint other = (CirclePoint) obj;
		if (largeCoordinate != other.largeCoordinate)
			return false;
		if (smallCoordinate != other.smallCoordinate)
			return false;
		return true;
	}
}
